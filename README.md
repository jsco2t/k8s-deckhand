# Introduction: k8s deckhand

[![pipeline status](https://gitlab.com/jsco2t/k8s-deckhand/badges/master/pipeline.svg)](https://gitlab.com/jsco2t/k8s-deckhand/-/commits/master)


K8S Deckhand has a simple goal: provide a rule based maintenance mechanism that runs within a `k8s` cluster. Rules are provided and managed using `CustomResourceDefinitions`. 

## Problem Statement:

Many _k8s clusters_, especially _non production_ clusters, are often used for CI/CD activities as well as providing runtime test environments. Over time these _"non prod"_ clusters have a tendency of gathering stale/dead resources. Cleaning up these resources can be done via multiple ways. This source repository is a POC of **one** way this cleanup can be performed. The goal is to provide a clean-up mechanism which runs within the cluster and can be managed via simple `kubectl` commands. 

## Project Status

> Authors Statement: _**I cannot state this clearly enough:** This is a POC for a solution to a problem domain. This solution is **NOT** production ready. The codebase is still under active development._

**High level feature list:**

- Use `CustomResourceDefinitions` to drive the maintenance activities this codebase will perform.
    - STATUS: functioning

- Code runs as a pod within the target k8s cluster.
    - STATUS: functioning

- Code supports scaling and/or deleting of resources (in specific namespaces).
    - STATUS: in-progress

- CI/CD Pipeline functioning in GitLab.
    - STATUS: partially-functional

- Automated test coverage supported in GitLab CI/CD pipeline
    - STATUS: in-progress

## Local Development:

This code should run within _Visual Studio_ or _Jetbrains Rider_, however the code only functions if you have a test k8s cluster which the code can communicate with. The biggest task in setting up a development environment is the setup of this test k8s cluster. There are multiple ways to get a test k8s cluster for this purpose. This document contains one option below in the section `Setting up a k8s test environment`.

## Q/A:

### Why use dotnet core?

The biggest reason is that I wanted some experience using the [official k8s csharp client](https://github.com/kubernetes-client/csharp). This example utility was an excellent way to build a simple dotnet service which allowed me to learn more about how the k8s csharp client works. 

### Why not use GO?

See ^^^ :) 

Though a more complete answer is: Given all the time I would write this solution in GO as well. Just to gain extra insight into the pro's/cons of writing these kinds of services for k8s. 


# Setting up a k8s test environment:

The `deckhand` k8s utility was tested using a k8s cluster built using `kubeadm`. `Kubeadm` was picked in this case as it provided a simplified way to setup a local development environment for k8s using vm's (vmware, parallels, hyper-v...etc)

The following provide a brief set of steps for setting up a local development environment. It's suggested using a cluster with two nodes (control plane, and worker node) as it best represents what a live runtime environment might look like. 

## Setup steps:

The following steps were performed on `Ubuntu Server 20.04`. Similar steps should work for a `RHEL` release. Please check the linked documents below for alternate commands you may need to run. 

- Install docker:

    ```
    sudo apt remove docker docker-engine docker.io containerd runc
    
    sudo apt install -y apt-transport-https ca-certificates curl gnupg lsb-release
    
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \ $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    
    sudo apt update && sudo apt install -y docker-ce docker-ce-cli containerd.io
    
    sudo usermod -aG docker {YOUR_CURRENT_USER}
    ```

- Install k8s tools:

    ```
    sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    
    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

    sudo apt update && sudo apt install -y kubelet kubeadm kubectl

    sudo apt-mark hold kubelet kubeadm kubectl
    ```

- Configure VM for k8s:

    - Shut off swap:

        ```
        sudo swapoff -a

        # verify with (should report 0):
        cat /proc/meminfo | grep 'SwapTotal'

        # edit the fstab file:
        sudo nvim /etc/fstab

        # remove this line (or one like it):
        /swap.img       none    swap    sw      0       0

        # lastly - remove the actual swap file:
        sudo rm /swap.img
        ```

    - Enable IP Forwarding (needed so that pods can access network):

        Edit:

        ```
        sudo vim /etc/sysctl.conf
        ```

        Find the following two items and uncomment them to enable them:

        ```
        net.ipv4.ip_forward=1
        net.ipv6.conf.all.forwarding=1
        ```

    - Change Docker to use `systemd` for management of the `cgroups` for docker: ([more info - see k8s docs](https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker))

        ```json
        cat <<EOF | sudo tee /etc/docker/daemon.json
        {
            "exec-opts": ["native.cgroupdriver=systemd"],
            "log-driver": "json-file",
            "log-opts": {
                "max-size": "100m"
            },
            "storage-driver": "overlay2"
        }
        EOF
        ```

    - Reload the docker service:

        ```
        sudo systemctl enable docker
        sudo systemctl daemon-reload
        sudo systemctl restart docker
        sudo systemctl status docker
        ```

    - Setup firewall rules (user choice - depending on dev environment - [more info](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports)):

        ```
        sudo ufw allow 22/tcp # for ssh access
        sudo ufw allow 6443/tcp
        sudo ufw allow 2379:2380/tcp
        sudo ufw allow 10250/tcp
        sudo ufw allow 10251/tcp
        sudo ufw allow 10252/tcp
        sudo ufw allow 30000:32767/tcp
        sudo ufw enable # starts the firewall
        sudo ufw status # gets status of the firewall
        ```
        * Note: the above ports include ports needed by the control plane and the nodes. See here for [more info](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports).

    - Adjust `br_netfilter` config so allow for bridge traffic (See here for [more info](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#letting-iptables-see-bridged-traffic))

        ```
        cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
        net.bridge.bridge-nf-call-ip6tables = 1
        net.bridge.bridge-nf-call-iptables = 1
        EOF
        sudo sysctl --system
        ```

    - Its recommended that (at least) the `control plane` nodes have a static IP address. The control plane(s) will need to have either a DNS name or a static IP address which is used as the _"advertise address"_. 
    
        Depending on the VM Platform the process for setting a static IP for a VM will differ. The following **example** is based on using _VMWare Fusion_ on _MacOS Big Sur_:

        Edit (elevated access): `/etc/bootptab`, and add to the file (**just an example**):
        ```bash
        #
        # bootptab
        #
        %%
        # Reservations have the following format:
        #
        # hostname hwtype hwaddr ipaddr bootfile
        k8s-control 1 00:50:REDACTED 192.168.155.50
        host k8s-node01 1 00:5C:REDACTED 192.168.155.51
        ```

        Restart VMWare Fusion network devices:
        ```
        sudo /Applications/VMware\ Fusion.app/Contents/Library/services/services.sh --stop
        sudo /Applications/VMware\ Fusion.app/Contents/Library/services/services.sh --start
        ```

        More info can be found [here](https://www.reddit.com/r/vmware/comments/krbz5m/fusion_12_dhcp_reservation/).

- Setup the control plane k8s node(s):

    We need to configure the control plan with _at least_ a few basic facts:
    - What IP Address it should use (advertise) for the kube api server
    - What virtual network cidr block to use for pods
    - There are many other parameters which can be provided. More info can be found [here](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/)

    As an **example**, the following was used to setup a dev environment control plane:
    ```
    sudo kubeadm init --pod-network-cidr=10.2.0.0/16 --apiserver-advertise-address 192.168.155.50
    ```

    It's recommended to first run the above command with the `--dry-run` parameter - just to see if any errors are reported:
    ```
    sudo kubeadm init --pod-network-cidr=10.2.0.0/16 --apiserver-advertise-address 192.168.155.50 --dry-run
    ```
    
    When `kubeadm` completes, the last few lines of output will need to be copied. It will be the command used on `worker nodes` to join them to the cluster. For example - this output may look like:

    ```
    kubeadm join 192.168.155.50:6443 --token qr...redacted \
    --discovery-token-ca-cert-hash sha256:b0d73e...redacted
    ```

- Setup kubectl on the control plane node:

    In the output of `kubeadm` it will report how to setup `kubectl` on the local VM so that it can talk to `k8s`. We will need this functionality so that we can setup the `networking CNI plugin` for `k8s`.

    The info should look similar to this:

    ```
    Your Kubernetes control-plane has initialized successfully!

    To start using your cluster, you need to run the following as a regular user:

    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```

    Run the above commands, and then run the following to verify that you can talk to the k8s cluster:

    ```
    kubectl get nodes
    ```

    The output should look similar to the following:
    
    ```
    NAME          STATUS     ROLES                  AGE     VERSION
    k8s-control   NotReady   control-plane,master   7m47s   v1.20.5
    ```

    The `NotReady` status above is currently expected. The control-plane won't function correctly until the `networking CNI plugin` is provided.

- Setup the Networking CNI Plugin on the control plane:

    There are multiple different Networking CNI Plugin's that are supported in k8s. For this dev environment we will use `calico`:

    Download (on the k8s control-plane node) the _quickstart_ k8s manifests for `calico`:

    ```
    curl https://docs.projectcalico.org/manifests/tigera-operator.yaml -O
    curl https://docs.projectcalico.org/manifests/custom-resources.yaml -O
    ```

    Edit the `custom-resources.yaml` file and make sure the `cidr` block specified matches the one supplied to `kubeadm` for the pod cidr block:

    ```
    apiVersion: operator.tigera.io/v1
    kind: Installation
    metadata:
    name: default
    spec:
    # Configures Calico networking.
    calicoNetwork:
        # Note: The ipPools section cannot be modified post-install.
        ipPools:
        - blockSize: 26
        cidr: 10.2.0.0/16               # <----------------------------- THIS!!!!
        encapsulation: VXLANCrossSubnet
        natOutgoing: Enabled
        nodeSelector: all()
    ```

    Apply the resources:
    ```
    kubectl apply -f tigera-operator.yaml
    kubectl apply -f custom-resources.yaml
    ```

    Wait for the `calico pods` to all be running:

    ```
    kubectl -n calico-system get pods --watch
    ```

    The end result should look similar to the following:

    ```
    NAME                                      READY   STATUS    RESTARTS   AGE
    calico-kube-controllers-f95867bfb-4xq9w   1/1     Running   0          58s
    calico-node-rgwdf                         1/1     Running   0          58s
    calico-typha-77bbbf9565-xfr9l             1/1     Running   0          58s
    ```

    After the above is complete - the `control-plane` node should now show that it's in a `ready` state:

    ```
    kubectl get nodes
    NAME          STATUS   ROLES                  AGE   VERSION
    k8s-control   Ready    control-plane,master   21m   v1.20.5
    ```

- Join the _worker node_ to the k8s cluster:

    - Log into the worker node(s) to join to the newly created cluster.

    - Run the `kubeadm join` command which the `control plane` node reported when you created it - it should look similar to this:

        ```
        kubeadm join 192.168.155.50:6443 --token qr...redacted --discovery-token-ca-cert-hash sha256:b0d73e...redacted
        ```

        When the command completes it should report an output similar to this:

        ```
        This node has joined the cluster:
        * Certificate signing request was sent to apiserver and a response was received.
        * The Kubelet was informed of the new secure connection details.
        ```

    - On the `control plane` you should now be able to see the newly added node come online:

        ```
        kubectl get nodes
        NAME          STATUS   ROLES                  AGE   VERSION
        k8s-control   Ready    control-plane,master   50m   v1.20.5
        k8s-node01    Ready    <none>                 11m   v1.20.5
        ```

## Setup documentation:

- Setup docker on the VM's:

    https://docs.docker.com/engine/install/ubuntu/

- Customize `cgroups` for docker to use `systemd`:

    https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker

- Setup kubeadm/kubelet/kubectl:

    https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

- Provision nodes:

    https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/

- Calico Quickstart / k8s CNI plugin:

    https://docs.projectcalico.org/getting-started/kubernetes/quickstart