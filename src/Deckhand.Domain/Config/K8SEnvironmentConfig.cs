using System;

namespace Deckhand.Domain.Config
{
    public class K8SEnvironmentConfig
    {
        private string configFilePath;

        public string ConfigFilePath
        {
            get => configFilePath;

            set => configFilePath = $"{AppContext.BaseDirectory}{value}";
        }

        public string CustomResourceGroup { get; set; }

        public string CustomResourceVersion { get; set; }

        public string CustomResourceType { get; set; }
    }
}
