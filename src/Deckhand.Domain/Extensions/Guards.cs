using System;

namespace Deckhand.Domain.Extensions
{
    public static class Guards
    {
        public static void GuardStringIsNotEmpty(this string itemToTest, string argumentName)
        {
            if (string.IsNullOrWhiteSpace(itemToTest))
            {
                throw new ArgumentException($"'{argumentName}' was found to be empty or null");
            }
        }
    }
}
