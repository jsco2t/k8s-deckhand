using Deckhand.Domain.Config;

namespace Deckhand.Domain.Extensions
{
    public static class ConfigExtensions
    {
        public static void ValidateConfig(this K8SEnvironmentConfig environmentConfig)
        {
            environmentConfig.ConfigFilePath.GuardStringIsNotEmpty(nameof(environmentConfig.ConfigFilePath));
        }
    }
}
