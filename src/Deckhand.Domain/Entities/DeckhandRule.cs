namespace Deckhand.Domain.Entities
{
    public class DeckhandRule
    {
        public string TargetNamespace { get; set; }

        public int MaxAgeHours { get; set; }

        public string TargetResourceType { get; set; }
    }
}
