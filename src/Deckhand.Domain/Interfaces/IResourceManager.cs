using System.Threading.Tasks;

namespace Deckhand.Domain.Interfaces
{
    public interface IResourceManager
    {
        public Task EvalResources();
    }
}
