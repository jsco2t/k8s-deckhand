using System.Collections.Generic;
using System.Threading.Tasks;

namespace Deckhand.Domain.Interfaces
{
    public interface IK8SController<T>
    {
        public IList<T> ListResources(string namespaceSearchTerm = null);

        public Task<IList<T>> ListResourcesAsync(string namespaceSearchTerm = null);
    }
}
