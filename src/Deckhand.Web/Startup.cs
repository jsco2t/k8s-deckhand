using Deckhand.Core.Managers;
using Deckhand.Domain.Interfaces;
using Deckhand.Web.Clients;
using Graceterm;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Deckhand.Web
{
    public partial class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddOptions()
                .AddLogging()
                .AddHostedService<ClientHost>();

            var loggingConfiguration = new LoggerConfiguration().ReadFrom.Configuration(this.Configuration);
            Log.Logger = loggingConfiguration.CreateLogger();

            ConfigureInfrastructure(services);
            services.AddSingleton(typeof(IResourceManager), typeof(K8SResourceManager));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging()
                .UseRouting()
                .UseGraceterm(options => options.IgnorePath("/healthcheck"));

            // app.UseEndpoints(endpoints =>
            // {
            //     endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
            // });
        }
    }
}
