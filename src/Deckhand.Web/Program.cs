using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Deckhand.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                Log.Fatal((Exception)eventArgs.ExceptionObject, "Application has crashed due to an un-handled exception");
                Log.CloseAndFlush();
                Environment.Exit(-1);
            };

            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Host startup has failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(
                    webBuilder =>
                    {
                        webBuilder.ConfigureAppConfiguration(ConfigureDelegate);
                        webBuilder.ConfigureLogging(
                            (context, builder) =>
                            {
                                builder.AddConfiguration(context.Configuration.GetSection("Logging"));
                                builder.AddConsole();
                            }
                        );
                        webBuilder.UseStartup<Startup>();
                    })
                .UseSerilog();

        private static void ConfigureDelegate(
            WebHostBuilderContext webHostBuilderContext,
            IConfigurationBuilder configurationBuilder
        )
        {
            var hostingEnvironment = webHostBuilderContext.HostingEnvironment;
            var envId = hostingEnvironment?.EnvironmentName;

            configurationBuilder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            if (!string.IsNullOrWhiteSpace(envId))
            {
                configurationBuilder.AddJsonFile($"appsettings.{envId}.json", true, true);
            }

            // needs to be last so that env vars (non secret) can be used as overrides at runtime
            configurationBuilder.AddEnvironmentVariables();
        }
    }
}
