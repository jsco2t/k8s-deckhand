using Deckhand.Domain.Config;
using Deckhand.Domain.Entities;
using Deckhand.Domain.Extensions;
using Deckhand.Domain.Interfaces;
using Deckhand.Infrastructure.Clients;
using Deckhand.Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Deckhand.Web
{
    public partial class Startup
    {
        private void ConfigureInfrastructure(IServiceCollection services)
        {
            var k8sConfig = new K8SEnvironmentConfig();
            Configuration.Bind("K8SConfig", k8sConfig);
            k8sConfig.ValidateConfig();

            services.AddSingleton(k8sConfig);

            // The k8s client api can be a bit heavy-weight, and I have observed in the past cases where
            // lifetime cleanup on the object would result in a deadlock (due to a combination of framework
            // issue and api issue. Our core use scenario for it will be in a service host which requires
            // the use of `singleton` here.
            services.AddSingleton(typeof(IKubernetesClient), typeof(KubernetesClient));

            // add controllers:
            services.AddSingleton(typeof(IK8SController<DeckhandRule>), typeof(DeckhandCustomResources));
        }
    }
}
