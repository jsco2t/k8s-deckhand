using System;
using System.Threading;
using System.Threading.Tasks;
using Deckhand.Domain.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Deckhand.Web.Clients
{
    public class ClientHost : IHostedService, IDisposable
    {
        private readonly ILogger<ClientHost> logger;
        private readonly IResourceManager k8SResourceManager;
        private readonly CancellationTokenSource runloopCts = new CancellationTokenSource();
        private Task executingTask;

        public ClientHost(IResourceManager k8SResourceManager, ILogger<ClientHost> logger)
        {
            this.logger = logger;
            this.k8SResourceManager = k8SResourceManager;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation($"Service '{nameof(ClientHost)}' is starting...");
            executingTask = ExecuteAsync(runloopCts.Token);

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation($"Stop has been requested for {nameof(ClientHost)}");

            if (null == executingTask)
            {
                // run loop already shut down (or never started) nothing to do:
                return;
            }

            try
            {
                runloopCts.Cancel();
            }
            finally
            {
                // run loop didn't shut down when requested, give it a couple seconds to exit or until the
                // cancellationToken is triggered.
                await Task.WhenAny(executingTask, Task.Delay(TimeSpan.FromSeconds(5), cancellationToken));
            }
        }

        public void Dispose()
        {
            runloopCts.Cancel();
        }

        private async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            // in the unlikely case that code in this method ends up
            // blocking we have this early (1ms) delay which allows
            // the rest of the app start-up code to run
            await Task.Delay(TimeSpan.FromMilliseconds(1), cancellationToken);

            logger.LogInformation($"{nameof(ClientHost)}: Run loop for service is starting...");
            cancellationToken.Register(() =>
                logger.LogInformation($"{nameof(ClientHost)}: Run loop for service is stopping..."));

            // run loop for service:
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(10), cancellationToken);
                logger.LogInformation($"{nameof(ClientHost)}: running...");
                await k8SResourceManager.EvalResources();
            }

            logger.LogInformation($"{nameof(ClientHost)}: Run loop for service has stopped");
        }
    }
}
