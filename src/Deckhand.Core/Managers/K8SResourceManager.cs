using System.Threading.Tasks;
using Deckhand.Domain.Entities;
using Deckhand.Domain.Interfaces;
using Microsoft.Extensions.Logging;

namespace Deckhand.Core.Managers
{
    public class K8SResourceManager : IResourceManager
    {
        private readonly IK8SController<DeckhandRule> deckhandRulesController;
        private readonly ILogger<K8SResourceManager> logger;

        public K8SResourceManager(IK8SController<DeckhandRule> deckhandRulesController, ILogger<K8SResourceManager> logger)
        {
            this.deckhandRulesController = deckhandRulesController;
            this.logger = logger;
        }

        public async Task EvalResources()
        {
            logger.LogInformation($"{nameof(K8SResourceManager)}: Running {nameof(EvalResources)}");
            var deckhandRules = await deckhandRulesController.ListResourcesAsync();

            logger.LogInformation($"{nameof(K8SResourceManager)}: Found {deckhandRules.Count.ToString()} deckhand rules");
            // TODO: Placeholder - need to perform the prescribed resource management provided by the
            // deckhand rules
        }
    }
}
