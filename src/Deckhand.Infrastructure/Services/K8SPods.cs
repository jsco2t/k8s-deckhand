using System.Collections.Generic;
using System.Threading.Tasks;
using Deckhand.Domain.Interfaces;
using Deckhand.Infrastructure.Clients;
using k8s;
using k8s.Models;

namespace Deckhand.Infrastructure.Services
{
    public class K8SPods : IK8SController<V1Pod>
    {
        private readonly IKubernetesClient client;

        public K8SPods(IKubernetesClient client)
        {
            this.client = client;
        }

        public IList<V1Pod> ListResources(string namespaceSearchTerm = "")
        {
            return ListResourcesAsync(namespaceSearchTerm).GetAwaiter().GetResult();
        }

        public async Task<IList<V1Pod>> ListResourcesAsync(string namespaceSearchTerm = "")
        {
            var result = await client.Service.ListNamespacedPodAsync(namespaceSearchTerm);

            return result.Items;
        }
    }
}
