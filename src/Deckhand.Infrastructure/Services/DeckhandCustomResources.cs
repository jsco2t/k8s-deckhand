using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Deckhand.Domain.Config;
using Deckhand.Domain.Entities;
using Deckhand.Domain.Interfaces;
using Deckhand.Infrastructure.Clients;
using Deckhand.Infrastructure.Entities;
using k8s;
using Microsoft.Extensions.Logging;

namespace Deckhand.Infrastructure.Services
{
    public class DeckhandCustomResources : IK8SController<DeckhandRule>
    {
        private readonly IKubernetesClient client;
        private readonly K8SEnvironmentConfig environmentConfig;
        private readonly ILogger<DeckhandCustomResources> logger;

        public DeckhandCustomResources(IKubernetesClient client, K8SEnvironmentConfig environmentConfig, ILogger<DeckhandCustomResources> logger)
        {
            this.client = client;
            this.environmentConfig = environmentConfig;
            this.logger = logger;
        }

        public IList<DeckhandRule> ListResources(string namespaceSearchTerm = null)
        {
            return ListResourcesAsync(namespaceSearchTerm).GetAwaiter().GetResult();
        }

        public async Task<IList<DeckhandRule>> ListResourcesAsync(string namespaceSearchTerm = null)
        {
            logger.LogInformation($"{nameof(DeckhandCustomResources)}: Running {nameof(ListResourcesAsync)}");

            var generic = new GenericClient(
                    client.Configuration,
                    environmentConfig.CustomResourceGroup,
                    environmentConfig.CustomResourceVersion,
                    environmentConfig.CustomResourceType
                );

            // var test = client.Service.ListClusterCustomObject("stable.jsco2t.com", "v1alpha1", "deckhands");
            var deckhandResources = await generic.ListAsync<CustomResourceList<DeckhandResource>>();

            if (null != deckhandResources)
            {
                return deckhandResources.Items.Select(resource => new DeckhandRule()
                {
                    TargetNamespace = resource.Spec.TargetNamespace,
                    MaxAgeHours = resource.Spec.MaxAgeHours,
                    TargetResourceType = resource.Spec.TargetResourceType
                }).ToList();
            }

            return new List<DeckhandRule>();
        }
    }
}
