using Newtonsoft.Json;

namespace Deckhand.Infrastructure.Entities
{
    public class DeckhandResourceSpec
    {
        [JsonProperty(PropertyName = "namespace")]
        public string TargetNamespace { get; set; }

        [JsonProperty(PropertyName = "maxAgeHours")]
        public int MaxAgeHours { get; set; }

        [JsonProperty(PropertyName = "resourceType")]
        public string TargetResourceType { get; set; }
    }
}
