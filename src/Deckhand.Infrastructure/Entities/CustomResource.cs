using k8s;
using k8s.Models;
using Newtonsoft.Json;

namespace Deckhand.Infrastructure.Entities
{
    public class CustomResource : KubernetesObject
    {
        [JsonProperty(PropertyName = "metadata")]
        public V1ObjectMeta Metadata { get; set; }
    }

    public abstract class CustomResource<TSpec, TStatus> : CustomResource
    {
        [JsonProperty(PropertyName = "spec")]
        public TSpec Spec { get; set; }

        [JsonProperty(PropertyName = "CStatus")]
        public TStatus CStatus { get; set; }
    }
}
