using System.Collections.Generic;
using k8s;
using k8s.Models;

namespace Deckhand.Infrastructure.Entities
{
    public class CustomResourceList<T> : KubernetesObject
        where T : CustomResource
    {
        public V1ListMeta Metadata { get; set; }
        public List<T> Items { get; set; }
    }
}
