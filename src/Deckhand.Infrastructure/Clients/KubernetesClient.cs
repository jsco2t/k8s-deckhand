using Deckhand.Domain.Config;
using k8s;

namespace Deckhand.Infrastructure.Clients
{
    public class KubernetesClient : IKubernetesClient
    {
        private IKubernetes client;
        private KubernetesClientConfiguration configuration;
        private readonly string filePathToK8SConfig;
        public IKubernetes Service => client ?? InitializeClient();

        public KubernetesClientConfiguration Configuration => configuration ?? InitializeConfiguration();

        public KubernetesClient(K8SEnvironmentConfig environmentConfig)
        {
            this.filePathToK8SConfig = environmentConfig.ConfigFilePath;
        }

        /*public KubernetesClient() : this($"{AppContext.BaseDirectory}/config/k8s.config")
        { }*/

        private IKubernetes InitializeClient()
        {
            client = new Kubernetes(Configuration);
            return client;
        }

        private KubernetesClientConfiguration InitializeConfiguration()
        {
            configuration = KubernetesClientConfiguration.BuildConfigFromConfigFile(filePathToK8SConfig);
            return configuration;
        }
    }
}
