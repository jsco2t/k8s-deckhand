using k8s;

namespace Deckhand.Infrastructure.Clients
{
    public interface IKubernetesClient
    {
        IKubernetes Service { get; }
        KubernetesClientConfiguration Configuration { get; }
    }
}
