#!/bin/bash

# docker vars
dockerImageName='registry.gitlab.com/jsco2t/k8s-deckhand'
dockerImageTag=''
dockerFilePath='src/Deckhand.Web/Dockerfile'
dockerContext='.'

# output formatting vars
outputSeparatorBar='#######################################################'
outputFormatSpace='  '

usage()
{
    echo $outputSeparatorBar
    echo "Script usage:"
    echo "$outputFormatSpace-i | --docker-image-name: docker image name *without* tag"
    echo "$outputFormatSpace-t | --docker image tag"
    echo "$outputFormatSpace-f | --docker-filepath: path to docker file"
    echo "$outputFormatSpace-c | --docker-build-context: docker build context, use '.' if not sure"
    echo "$outputFormatSpace-h | --help: show this help info"
    echo $outputSeparatorBar
    echo ""
}

show_config()
{
    echo $outputSeparatorBar
    echo "Script is using the following parameter values:"
    echo "$outputFormatSpace Docker Image Name: $dockerImageName"
    echo "$outputFormatSpace Docker Image Tag: $dockerImageTag"
    echo "$outputFormatSpace Docker File: $dockerFilePath"
    echo "$outputFormatSpace Docker Context: $dockerContext"
    echo $outputSeparatorBar
    echo ""
}

for i in "$@"
do
    case $i in
        -i | --docker-image-name )
            shift
            dockerImageName="$1"
            ;;
        -t | --docker-image-tag )
            shift
            dockerImageTag="$1"
            ;;
        -f | --docker-filepath )
            shift
            dockerFilePath=$1
            ;;
        -c | --docker-build-context )
            shift
            dockerContext=$1
            ;;
        -h | --help )
            shift
            usage
            exit
            ;;
        * )
            if [ "$1" != "" ]; then
                echo ""
                echo "Parameter '$1' is not recognized..."
                echo ""
                usage
                exit -1
            fi
    esac
    shift
done

# show runtime configuration
show_config

if [ "$dockerImageTag" == "" ]; then
    dockerImageTag="$(git rev-parse --short HEAD)"
    echo "Docker Image Tag has been set to: $dockerImageTag"
fi

# build docker image
docker build -t "$dockerImageName:$dockerImageTag" -f $dockerFilePath $dockerContext
docker tag "$dockerImageName:$dockerImageTag" "$dockerImageName:latest"
docker push "$dockerImageName:$dockerImageTag" 
docker push "$dockerImageName:latest"